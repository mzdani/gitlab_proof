This is an OpenPGP proof that connects [my OpenPGP key](https://keyoxide.org/6B00F7DDC992E9468606D2A8E451BA19D2DCFA61) to [this Github account](https://gitlab.com/mzdani). For details check out https://docs.keyoxide.org/advanced/openpgp-proofs/

[Verifying my OpenPGP key: openpgp4fpr:6B00F7DDC992E9468606D2A8E451BA19D2DCFA61]
